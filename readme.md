React.js - demos
=======

this is my [React.js](http://facebook.github.io/react/) playground

examples
------

* __autocomplete__
    - simple autocomplete demo - based on csfd.cz unofficial api [demo](http://react.twista.cz/autocomplete/)
* __countableBasket__
    - simple demo with input and + and - buttons [demo](http://react.twista.cz/countableBasket/)
* __dataTable__
    - sortable and paginationable table based on given JSON [demo](http://react.twista.cz/dataTable/)
* __tree viewer__
    - simple tree viewer example [demo](http://react.twista.cz/TreeViewer/)
* __image slider__
    - simple configurable image slider (without transitions) [demo](http://react.twista.cz/ImageSlider/)

